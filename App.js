import Expo from 'expo';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';

import AuthScreen from './screens/AuthScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import DeckScreen from './screens/DeckScreen';
import MapScreen from './screens/MapScreen';
import ReviewScreen from './screens/ReviewScreen';
import SettingsScreen from './screens/SettingsScreen';



class App extends Component {
  
  render() {  
    const MainNavigator = TabNavigator({
      welcome: { screen: WelcomeScreen },
      auth: { screen: AuthScreen},
      main: {
        screen: TabNavigator({
          map: { screen: MapScreen},
          deck: {screen: DeckScreen}, 
          review: {
            screen: StackNavigator({
              review: { screen: ReviewScreen},
              setting: {screen: SettingsScreen}
            })
          }
        })
      }
    });

    return (
      <View style={styles.container}>
        <MainNavigator /> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;